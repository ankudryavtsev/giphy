package me.akudryavtsev.giphy.presentation.adapter

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import me.akudryavtsev.giphy.model.data.GifObject

class GifAdapter(private val onGifClickedListener: OnGifClickedListener) :
    PagedListAdapter<GifObject, GifViewHolder>(diffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): GifViewHolder =
        GifViewHolder(parent, onGifClickedListener)

    override fun onBindViewHolder(viewHolder: GifViewHolder, position: Int) {
        val gifObject = getItem(position)
        with(viewHolder) {
            gifObject?.let {
                bind(it)
            }
        }
    }

    companion object {
        /**
         * This diff callback informs the PagedListAdapter how to compute list differences when new
         * PagedLists arrive.
         */
        private val diffCallback = object : DiffUtil.ItemCallback<GifObject>() {
            override fun areItemsTheSame(oldItem: GifObject, newItem: GifObject): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: GifObject, newItem: GifObject): Boolean =
                oldItem == newItem
        }
    }
}
