package me.akudryavtsev.giphy.presentation.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import me.akudryavtsev.giphy.R
import me.akudryavtsev.giphy.model.data.GifObject

class GifViewHolder(parent: ViewGroup, private val onGifClickedListener: OnGifClickedListener) :
    androidx.recyclerview.widget.RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.gif_item,
            parent,
            false
        )
    ) {

    private val imageView = itemView.findViewById<ImageView>(R.id.image_view)

    fun bind(gifObject: GifObject) {
        imageView.setOnClickListener { onGifClickedListener.onGifClicked(gifObject) }
        Glide.with(itemView).clear(itemView)
        imageView.setImageDrawable(null)
        gifObject.images?.fixedHeight?.url?.let { url ->
            Log.d("GifViewHolder", url)
            Glide.with(itemView)
                .load(url)
                .apply(
                    RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.error)
                )
                .into(imageView)
                .clearOnDetach()
        }

    }
}
