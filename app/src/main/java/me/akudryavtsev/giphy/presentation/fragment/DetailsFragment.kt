package me.akudryavtsev.giphy.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import me.akudryavtsev.giphy.R
import me.akudryavtsev.giphy.model.data.GifObject
import me.akudryavtsev.giphy.presentation.viewmodel.DetailsViewModel
import me.akudryavtsev.giphy.presentation.viewmodel.SharedViewModel
import me.akudryavtsev.giphy.presentation.viewmodel.factory.GiphyViewModelFactory
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class DetailsFragment : Fragment(), KodeinAware {

    override val kodein by kodein { requireActivity() }
    private val detailsFragmentArgs: DetailsFragmentArgs by navArgs()

    private lateinit var imageView: ImageView
    private lateinit var nicknameView: TextView
    private lateinit var usernameView: TextView
    private lateinit var twitterNameView: TextView
    private lateinit var profileUrlView: TextView

    private val giphyViewModelFactory: GiphyViewModelFactory by instance()
    private lateinit var sharedViewModel: SharedViewModel
    private lateinit var viewModel: DetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_details, container, false)
        imageView = view.findViewById(R.id.image_view)
        nicknameView = view.findViewById(R.id.nickname_view)
        usernameView = view.findViewById(R.id.username_view)
        twitterNameView = view.findViewById(R.id.twitter_name_view)
        profileUrlView = view.findViewById(R.id.profile_url_view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedViewModel = activity?.run {
            ViewModelProviders.of(this, giphyViewModelFactory).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        viewModel = ViewModelProviders.of(this, giphyViewModelFactory).get(DetailsViewModel::class.java)
        viewModel.gifDetails.observe(this, Observer { gifObject -> render(gifObject) })
        viewModel.init(detailsFragmentArgs.id)
        sharedViewModel.selected.value
    }

    private fun render(gifObject: GifObject?) {
        gifObject?.images?.fixedHeight?.url?.let { showGif(it) }
        gifObject?.user?.display_name?.let { nicknameView.text = it }
        gifObject?.user?.username?.let { usernameView.text = it }
        gifObject?.user?.twitter?.let { twitterNameView.text = it }
        gifObject?.user?.profile_url?.let { profileUrlView.text = it }
    }

    private fun showGif(url: String) {
        Glide.with(this)
            .load(url)
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.error)
            )
            .into(imageView)
    }
}
