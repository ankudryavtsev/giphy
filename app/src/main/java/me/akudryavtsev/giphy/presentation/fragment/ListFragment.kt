package me.akudryavtsev.giphy.presentation.fragment

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.paging.PagedList
import me.akudryavtsev.giphy.R
import me.akudryavtsev.giphy.model.data.GifObject
import me.akudryavtsev.giphy.model.data.NetworkState
import me.akudryavtsev.giphy.presentation.adapter.GifAdapter
import me.akudryavtsev.giphy.presentation.adapter.OnGifClickedListener
import me.akudryavtsev.giphy.presentation.viewmodel.ListViewModel
import me.akudryavtsev.giphy.presentation.viewmodel.SharedViewModel
import me.akudryavtsev.giphy.presentation.viewmodel.factory.GiphyViewModelFactory
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

const val SPAN_COUNT_FOR_LANDSCAPE = 3
const val SPAN_COUNT_FOR_PORTRAIT = 2

class ListFragment : Fragment(), KodeinAware, OnGifClickedListener {

    override val kodein by kodein { requireActivity() }
    private val giphyViewModelFactory: GiphyViewModelFactory by instance()
    private lateinit var viewModel: ListViewModel
    private lateinit var sharedViewModel: SharedViewModel
    private val recyclerViewAdapter = GifAdapter(this)
    private lateinit var recyclerView: androidx.recyclerview.widget.RecyclerView
    private lateinit var swipeRefreshLayout: androidx.swiperefreshlayout.widget.SwipeRefreshLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh)
        recyclerView = view.findViewById(R.id.recycler_view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        swipeRefreshLayout.setOnRefreshListener { viewModel.update() }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedViewModel = activity?.run {
            ViewModelProviders.of(this, giphyViewModelFactory).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        viewModel = ViewModelProviders.of(this, giphyViewModelFactory).get(ListViewModel::class.java)
        viewModel.gifs.observe(this, Observer { pagedNoteList ->
            pagedNoteList?.let { render(pagedNoteList) }
        })
        viewModel.initialLoadState?.observe(this, Observer { processState(it) })
        viewModel.rangeLoadState?.observe(this, Observer { processState(it) })
    }

    override fun onGifClicked(gifObject: GifObject) {
        sharedViewModel.select(gifObject)
        gifObject.id?.let { findNavController(this).navigate(ListFragmentDirections.actionOpenGif(it)) }
    }

    private fun processState(networkState: NetworkState?) {
        if (networkState?.status == NetworkState.Status.ERROR)
            Toast.makeText(
                context,
                networkState.message,
                Toast.LENGTH_LONG
            ).show()
    }

    private fun setupRecyclerView() {
        recyclerView.adapter = recyclerViewAdapter
        val spanCount =
            if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) SPAN_COUNT_FOR_LANDSCAPE else SPAN_COUNT_FOR_PORTRAIT
        val gridLayoutManager = androidx.recyclerview.widget.GridLayoutManager(context, spanCount)
        recyclerView.layoutManager = gridLayoutManager
    }

    private fun render(pagedNoteList: PagedList<GifObject>) {
        recyclerViewAdapter.submitList(pagedNoteList)
        swipeRefreshLayout.isRefreshing = false
    }
}
