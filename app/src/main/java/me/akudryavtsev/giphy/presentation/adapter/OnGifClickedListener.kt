package me.akudryavtsev.giphy.presentation.adapter

import me.akudryavtsev.giphy.model.data.GifObject

/**
 *
 * @author Andrey Kudryavtsev on 2019-02-06.
 */
interface OnGifClickedListener {
    fun onGifClicked(gifObject: GifObject)
}