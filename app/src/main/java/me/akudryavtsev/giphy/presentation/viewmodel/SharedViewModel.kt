package me.akudryavtsev.giphy.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import me.akudryavtsev.giphy.model.data.GifObject

class SharedViewModel : ViewModel() {
    val selected = MutableLiveData<GifObject>()

    fun select(item: GifObject) {
        selected.value = item
    }
}