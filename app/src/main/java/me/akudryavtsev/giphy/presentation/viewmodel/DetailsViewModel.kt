package me.akudryavtsev.giphy.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import me.akudryavtsev.giphy.domain.GiphyInteractor
import me.akudryavtsev.giphy.model.data.GifObject

const val TAG = "DetailsViewModel"

/**
 *
 * @author Andrey Kudryavtsev on 2019-02-08.
 */
class DetailsViewModel(private val giphyInteractor: GiphyInteractor) : ViewModel() {

    val gifDetails = MutableLiveData<GifObject>()
    private var disposable: Disposable? = null

    fun init(id: String) {
        dispose()
        disposable =
            giphyInteractor.getGif(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ gifDetails.postValue(it) },
                    { throwable -> Log.e(TAG, "init ${throwable.message}", throwable) })
    }

    override fun onCleared() {
        super.onCleared()
        dispose()
    }

    private fun dispose() {
        disposable?.dispose()
        disposable = null
    }
}