package me.akudryavtsev.giphy.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import me.akudryavtsev.giphy.data.GiphyDataSourceFactory
import me.akudryavtsev.giphy.model.data.GifObject

private const val PAGE_SIZE = 10

/**
 *
 * @author Andrey Kudryavtsev on 2019-02-01.
 */
class ListViewModel(
    private val giphyDataSourceFactory: GiphyDataSourceFactory
) : ViewModel() {

    val gifs: LiveData<PagedList<GifObject>> = LivePagedListBuilder(
        giphyDataSourceFactory,
        PAGE_SIZE
    ).build()

    val initialLoadState = giphyDataSourceFactory.giphyDataSource?.initialLoadStateLiveData

    val rangeLoadState = giphyDataSourceFactory.giphyDataSource?.rangeLoadStateLiveData

    fun update() {
        gifs.value?.dataSource?.invalidate()
    }

    override fun onCleared() {
        super.onCleared()
        giphyDataSourceFactory.giphyDataSource?.clear()
    }
}