package me.akudryavtsev.giphy.presentation.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import me.akudryavtsev.giphy.data.GiphyDataSourceFactory
import me.akudryavtsev.giphy.domain.GiphyInteractor
import me.akudryavtsev.giphy.presentation.viewmodel.DetailsViewModel
import me.akudryavtsev.giphy.presentation.viewmodel.ListViewModel
import me.akudryavtsev.giphy.presentation.viewmodel.SharedViewModel

/**
 *
 * @author Andrey Kudryavtsev on 2019-02-01.
 */
class GiphyViewModelFactory(
    private val giphyInteractor: GiphyInteractor,
    private val giphyDataSourceFactory: GiphyDataSourceFactory
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ListViewModel::class.java)) {
            return ListViewModel(giphyDataSourceFactory) as T
        }
        if (modelClass.isAssignableFrom(DetailsViewModel::class.java)) {
            return DetailsViewModel(giphyInteractor) as T
        }
        if (modelClass.isAssignableFrom(SharedViewModel::class.java)) {
            return SharedViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}