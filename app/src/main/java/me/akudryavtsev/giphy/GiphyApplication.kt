package me.akudryavtsev.giphy

import android.app.Application
import me.akudryavtsev.giphy.data.GiphyDataSourceFactory
import me.akudryavtsev.giphy.data.GiphyRepository
import me.akudryavtsev.giphy.data.RestApi
import me.akudryavtsev.giphy.domain.GiphyInteractor
import me.akudryavtsev.giphy.domain.IGiphyRepository
import me.akudryavtsev.giphy.presentation.viewmodel.factory.GiphyViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

/**
 *
 * @author Andrey Kudryavtsev on 2019-02-08.
 */
class GiphyApplication : Application(), KodeinAware {
    override val kodein: Kodein = Kodein.lazy {
        import(androidXModule(this@GiphyApplication))
        bind<RestApi>() with singleton { RestApi.getApi(instance(tag = "cache")) }
        bind<IGiphyRepository>() with provider { GiphyRepository(instance()) }
        bind<GiphyInteractor>() with provider { GiphyInteractor(instance()) }
        bind<GiphyDataSourceFactory>() with provider { GiphyDataSourceFactory(instance()) }
        bind<GiphyViewModelFactory>() with provider { GiphyViewModelFactory(instance(), instance()) }
    }
}