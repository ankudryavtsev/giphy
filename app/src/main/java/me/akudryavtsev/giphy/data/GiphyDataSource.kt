package me.akudryavtsev.giphy.data

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PositionalDataSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import me.akudryavtsev.giphy.domain.IGiphyRepository
import me.akudryavtsev.giphy.model.data.GifObject
import me.akudryavtsev.giphy.model.data.NetworkState
import me.akudryavtsev.giphy.model.data.Trending

const val TAG = "GiphyDataSource"

/**
 *
 * @author Andrey Kudryavtsev on 2019-01-31.
 */
class GiphyDataSource(private val giphyRepository: IGiphyRepository) : PositionalDataSource<GifObject>() {

    val initialLoadStateLiveData = MutableLiveData<NetworkState>()
    val rangeLoadStateLiveData = MutableLiveData<NetworkState>()

    private val defaultList = emptyList<GifObject>()
    private val defaultPosition = 0
    private val defaultCount = 0

    private var compositeDisposable = CompositeDisposable()

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<GifObject>) {
        Log.d(TAG, "loadRange(${params.loadSize}, ${params.startPosition})")
        val trending = giphyRepository.trending(limit = params.loadSize, offset = params.startPosition)
        val disposable = trending
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result -> onRangeSuccessLoad(result, callback) },
                { throwable -> onRangeError(throwable, callback) })
        compositeDisposable.add(disposable)
    }

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<GifObject>) {
        Log.d(TAG, "loadInitial(${params.requestedLoadSize}, ${params.requestedStartPosition})")
        val trending =
            giphyRepository.trending(limit = params.requestedLoadSize, offset = params.requestedStartPosition)
        val disposable = trending
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result -> onInitialSuccessLoad(result, callback) },
                { throwable -> onInitialError(throwable, callback) })
        compositeDisposable.add(disposable)
    }

    fun clear() {
        compositeDisposable.clear()
        compositeDisposable = CompositeDisposable()
    }

    private fun onRangeSuccessLoad(trending: Trending, loadRangeCallback: LoadRangeCallback<GifObject>) {
        rangeLoadStateLiveData.postValue(NetworkState(NetworkState.Status.SUCCESS))
        loadRangeCallback.onResult(trending.gifs.toMutableList())
    }

    private fun onInitialSuccessLoad(trending: Trending, loadInitialCallback: LoadInitialCallback<GifObject>) {
        initialLoadStateLiveData.postValue(NetworkState(NetworkState.Status.SUCCESS))
        val pagination = trending.pagination
        loadInitialCallback.onResult(trending.gifs.toMutableList(), pagination.offset, pagination.total_count)
    }

    private fun onRangeError(throwable: Throwable, loadRangeCallback: LoadRangeCallback<GifObject>) {
        Log.e(TAG, "onRangeError ${throwable.message}", throwable)
        rangeLoadStateLiveData.postValue(NetworkState(NetworkState.Status.ERROR, throwable.message))
        loadRangeCallback.onResult(defaultList)
    }

    private fun onInitialError(throwable: Throwable, loadInitialCallback: LoadInitialCallback<GifObject>) {
        Log.e(TAG, "onInitialError ${throwable.message}", throwable)
        initialLoadStateLiveData.postValue(NetworkState(NetworkState.Status.ERROR, throwable.message))
        loadInitialCallback.onResult(defaultList, defaultPosition, defaultCount)
    }
}