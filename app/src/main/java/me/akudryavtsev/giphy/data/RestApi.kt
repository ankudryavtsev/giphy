package me.akudryavtsev.giphy.data

import io.reactivex.Single
import me.akudryavtsev.giphy.model.data.GifById
import me.akudryavtsev.giphy.model.data.Trending
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.io.File

const val CACHE_SIZE = 10 * 1024 * 1024L
const val API_KEY = "adnipWTtecj1tPIjZWvyNrsOh6JVTMMF"

/**
 *
 * @author Andrey Kudryavtsev on 2019-01-31.
 */
interface RestApi {
    @GET("/v1/gifs/trending")
    fun trending(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("apiKey") apiKey: String = API_KEY
    ): Single<Trending>

    @GET("/v1/gifs/{gifId}")
    fun gif(
        @Path("gifId") gifId: String,
        @Query("apiKey") apiKey: String = API_KEY
    ): Single<GifById>

    companion object {
        fun getApi(cacheDir: File): RestApi {
            val client = OkHttpClient.Builder()
                .cache(Cache(cacheDir, CACHE_SIZE))
                .build()
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.giphy.com/")
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(RestApi::class.java)
        }
    }
}