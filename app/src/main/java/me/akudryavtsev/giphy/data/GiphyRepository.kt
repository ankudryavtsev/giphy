package me.akudryavtsev.giphy.data

import io.reactivex.Single
import me.akudryavtsev.giphy.domain.IGiphyRepository
import me.akudryavtsev.giphy.model.data.GifById
import me.akudryavtsev.giphy.model.data.Trending

/**
 *
 * @author Andrey Kudryavtsev on 2019-02-01.
 */
class GiphyRepository(private val api: RestApi) : IGiphyRepository {

    override fun getGif(gifId: String): Single<GifById> = api.gif(gifId)

    override fun trending(limit: Int, offset: Int): Single<Trending> = api.trending(limit, offset)
}