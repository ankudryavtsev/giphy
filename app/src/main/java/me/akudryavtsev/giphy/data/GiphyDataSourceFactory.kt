package me.akudryavtsev.giphy.data

import androidx.paging.DataSource
import me.akudryavtsev.giphy.domain.IGiphyRepository
import me.akudryavtsev.giphy.model.data.GifObject


/**
 *
 * @author Andrey Kudryavtsev on 2019-02-01.
 */
class GiphyDataSourceFactory(private val giphyRepository: IGiphyRepository) : DataSource.Factory<Int, GifObject>() {

    var giphyDataSource: GiphyDataSource? = null

    override fun create(): DataSource<Int, GifObject> {
        giphyDataSource = GiphyDataSource(giphyRepository)
        return giphyDataSource as DataSource<Int, GifObject>
    }
}