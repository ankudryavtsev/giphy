package me.akudryavtsev.giphy.model.data

import com.google.gson.annotations.SerializedName

/**
 *
 * @author Andrey Kudryavtsev on 2019-01-31.
 */
data class Trending(

    @field:SerializedName("data")
    val gifs: List<GifObject>,

    @field:SerializedName("pagination")
    val pagination: PaginationObject,

    @field:SerializedName("meta")
    val meta: MetaObject
)

data class MetaObject(
    val msg: String,
    val status: Int,
    val response_id: String?
)

data class PaginationObject(
    val offset: Int,
    val total_count: Int,
    val count: Int
)