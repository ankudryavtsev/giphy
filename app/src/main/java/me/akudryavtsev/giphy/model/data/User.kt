package me.akudryavtsev.giphy.model.data

import com.google.gson.annotations.SerializedName

/**
 *
 * @author Andrey Kudryavtsev on 2019-01-31.
 */
data class User(
    @field:SerializedName("avatar_url")
    val avatar_url: String?,
    @field:SerializedName("banner_url")
    val banner_url: String?,
    @field:SerializedName("profile_url")
    val profile_url: String?,
    @field:SerializedName("username")
    val username: String?,
    @field:SerializedName("display_name")
    val display_name: String?,
    @field:SerializedName("twitter")
    val twitter: String?
)