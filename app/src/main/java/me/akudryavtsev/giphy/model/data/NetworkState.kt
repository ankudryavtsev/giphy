package me.akudryavtsev.giphy.model.data

/**
 *
 * @author Andrey Kudryavtsev on 2019-02-11.
 */
data class NetworkState(
    val status: Status,
    val message: String? = null
) {
    enum class Status {
        LOADING, SUCCESS, ERROR
    }
}