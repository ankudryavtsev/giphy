package me.akudryavtsev.giphy.model.data

import com.google.gson.annotations.SerializedName
import me.akudryavtsev.giphy.model.data.size.*

data class Images(

    @field:SerializedName("original")
    val original: Original? = null,

    @field:SerializedName("fixed_height_small")
    val fixedHeightSmall: FixedHeightSmall? = null,

    @field:SerializedName("fixed_width")
    val fixedWidth: FixedWidth? = null,

    @field:SerializedName("downsized_still")
    val downsizedStill: DownsizedStill? = null,

    @field:SerializedName("downsized_large")
    val downsizedLarge: DownsizedLarge? = null,

    @field:SerializedName("fixed_height_downsampled")
    val fixedHeightDownsampled: FixedHeightDownsampled? = null,

    @field:SerializedName("fixed_height")
    val fixedHeight: FixedHeight? = null,

    @field:SerializedName("fixed_width_downsampled")
    val fixedWidthDownsampled: FixedWidthDownsampled? = null,

    @field:SerializedName("fixed_height_small_still")
    val fixedHeightSmallStill: FixedHeightSmallStill? = null,

    @field:SerializedName("fixed_width_small")
    val fixedWidthSmall: FixedWidthSmall? = null,

    @field:SerializedName("fixed_height_still")
    val fixedHeightStill: FixedHeightStill? = null,

    @field:SerializedName("fixed_width_small_still")
    val fixedWidthSmallStill: FixedWidthSmallStill? = null,

    @field:SerializedName("fixed_width_still")
    val fixedWidthStill: FixedWidthStill? = null,

    @field:SerializedName("downsized")
    val downsized: Downsized? = null,

    @field:SerializedName("original_still")
    val originalStill: OriginalStill? = null
)