package me.akudryavtsev.giphy.model.data

import com.google.gson.annotations.SerializedName

/**
 *
 * @author Andrey Kudryavtsev on 2019-02-08.
 */
data class GifById(

    @field:SerializedName("data")
    val gif: GifObject,

    @field:SerializedName("meta")
    val meta: MetaObject
)