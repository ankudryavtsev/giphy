package me.akudryavtsev.giphy.model.data


import com.google.gson.annotations.SerializedName


data class OriginalStill(

    @field:SerializedName("width")
    val width: String? = null,

    @field:SerializedName("url")
    val url: String? = null,

    @field:SerializedName("height")
    val height: String? = null
)