package me.akudryavtsev.giphy.domain

import io.reactivex.Single
import me.akudryavtsev.giphy.model.data.GifById
import me.akudryavtsev.giphy.model.data.Trending

/**
 *
 * @author Andrey Kudryavtsev on 2019-02-01.
 */
interface IGiphyRepository {

    fun getGif(gifId: String): Single<GifById>

    fun trending(limit: Int = 25, offset: Int = 0): Single<Trending>
}