package me.akudryavtsev.giphy.domain

import io.reactivex.Single
import me.akudryavtsev.giphy.model.data.GifObject

/**
 *
 * @author Andrey Kudryavtsev on 2019-02-01.
 */
class GiphyInteractor(private val giphyRepository: IGiphyRepository) {

    fun getGif(gifId: String): Single<GifObject> {
        return giphyRepository.getGif(gifId).map { it.gif }
    }
}